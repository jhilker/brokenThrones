const defaultTheme = require("tailwindcss/defaultTheme")

module.exports = {
  content: [
    "layouts/**/*.html/",
    "content/**/*/.md"

  ],
  darkMode: 'class',
  theme: {
    gridTemplateAreas: {
      mobile: ['header header header', 'main main main', 'footer footer footer'],
      desktop: ['sidebar header header', 'sidebar main main', 'sidebar footer footer'],
    },
    extend: {
      gridTemplateColumns: {
        layout: '0.7fr 2.3fr 1fr',
      },
      gridTemplateRows: {
        layout: '0.2fr 2.6fr 0.2fr',
      },
      screens: {
        mobile: { max: '1023px'}
      },

      colors: {
        manuwhite: "#f0e3d1",
        manutan: "#d0c4a9",
        heraldic: {
          red: "#bc2e2e",
          blue: {
            DEFAULT: "#0d6793",
            bright: "#1188c2"
          },
        },
      },
      fontFamily: {
        alegreya: ["Alegreya", defaultTheme.fontFamily.serif]
      },
    },
  },
  plugins: [
    require('tailwind-scrollbar'),
    require('@savvywombat/tailwindcss-grid-areas'),
    require('@tailwindcss/forms'),
    require('@tailwindcss/typography')
  ],
}
